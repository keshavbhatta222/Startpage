/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */

/**
 * Search function
 */

const searchInput = document.querySelector("#searchbar > input")
const searchButton = document.querySelector("#searchbar > button")

const lookup = {"/":"/","deepl":"https://deepl.com/","reddit":"https://reddit.com/","maps":"https://maps.google.com/","searx":"https://searx.be/searchq="}
const engine = "searx"
const engineUrls = {
  deepl: "https://www.deepl.com/translator#-/-/",
  duckduckgo: "https://duckduckgo.com/?q=",
  ecosia: "https://www.ecosia.org/search?q=",
  google: "https://www.google.com/search?q=",
  startpage: "https://www.startpage.com/search?q=",
  youtube: "https://www.youtube.com/results?q=",
  searx: "https://searx.be/search?q=",
}

const isWebUrl = value => {
  try {
    const url = new URL(value)
    return url.protocol === "http:" || url.protocol === "https:"
  } catch {
    return false
  }
}

const getTargetUrl = value => {
  if (isWebUrl(value)) return value
  if (lookup[value]) return lookup[value]
  return engineUrls[engine] + value
}

const search = () => {
  const value = searchInput.value
  const targetUrl = getTargetUrl(value)
  window.open(targetUrl, "_self")
}

searchInput.onkeyup = event => event.key === "Enter" && search()
searchButton.onclick = search

/**
 * inject bookmarks into html
 */

const bookmarks = [{"id":"cSlv20ZnMlFeFljl","label":"Favweb","bookmarks":[{"id":"RriH3vZ3drV8i8po","label":"Youtube","url":"https://www.youtube.com/"},{"id":"BUqpZFwUzMo7L8ny","label":"Twitter","url":"https://twitter.com/home"},{"id":"RQNUp7UkzOTDcny6","label":"Facebook","url":"https://www.facebook.com/"},{"id":"5V4Vq43IBCrKSSoH","label":"Reddit","url":"https://www.reddit.com/"}]},{"id":"POTxdNoaEv6MMIGH","label":"reddit","bookmarks":[{"id":"Udg16i1TBdQMGRcz","label":"r/linux","url":"https://www.reddit.com/r/linux/"},{"id":"q22lS4Oaolj3YMUx","label":"r/unixporn","url":"https://www.reddit.com/r/unixporn/"},{"id":"I7XnQ9E0F8rkV0YO","label":"r/gnu","url":"https://www.reddit.com/r/gnu/"},{"id":"Lxjnu7HrwEktvxbg","label":"r/debian","url":"https://www.reddit.com/r/debian/"}]},{"id":"MJ1zfZM4ddxvvMxH","label":"Gitlab","bookmarks":[{"id":"ucrjC5IF3ZzDXwf4","label":"Mine","url":"https://gitlab.com/keshavbhatta222"},{"id":"bDo7icDZpTEIzQwI","label":"Matts","url":"https://gitlab.com/thelinuxcast"},{"id":"GhhnKA5OnYqGaHfV","label":"Dereks","url":"https://gitlab.com/dwt1"},{"id":"aUI7BQHeqAUrDq8E","label":"Nala","url":"https://gitlab.com/volian/nala"}]},{"id":"65rcbURBdBNFrsC3","label":"anime","bookmarks":[{"id":"j7xH1heGuPpUtpYy","label":"zoro","url":"https://zoro.to/home"},{"id":"QL6wUqnuyvlgWG8g","label":"animixplay","url":"https://animixplay.to/?from=com"},{"id":"FHWifYvo869XvPW7","label":"animepahe","url":"https://animepahe.com/"},{"id":"bvXGk1MlkYCVHIEx","label":"9anime","url":"https://9anime.id/home"}]},{"id":"ghCCrNxk1GVwzImh","label":"Manga","bookmarks":[{"id":"LY4ZxXELSCn0xa6m","label":"Mangareader","url":"https://mangareader.to/home"},{"id":"2KL0otW4wNIbWeYO","label":"Manga4life","url":"https://manga4life.com/"},{"id":"hDW4aj6GwIExnc5G","label":"Mangakakalot","url":"https://mangakakalot.com/"},{"id":"XIL4Y9XG4KGIPH8v","label":"TCBScans","url":"https://onepiecechapters.com/"}]},{"id":"oOu2dxKwVB3p66E3","label":"github","bookmarks":[{"id":"sKZXp7os1Qqb93IQ","label":"startpage","url":"https://github.com/deepjyoti30/startpage"},{"id":"jgZcbidJzvkAoour","label":"generic startpage","url":"https://github.com/PrettyCoffee/yet-another-generic-startpage"},{"id":"jIk6UDqFar7LPAdA","label":"xmonad","url":"https://github.com/xmonad/xmonad"},{"id":"DHQyxmnriJitEL66","label":"christitus","url":"https://github.com/ChrisTitusTech"}]},{"id":"2lstix1ZTxbnjH02","label":"Distros","bookmarks":[{"id":"i0HoSeGPmFAHdZN3","label":"Debian","url":"https://www.debian.org/"},{"id":"0TYaIhKPO6oOt8Xk","label":"Arch","url":"https://archlinux.org/"},{"id":"3ryswZuK27wED1Nt","label":"Arco","url":"https://arcolinux.com/"},{"id":"fpCeyMdqTNHHMtWB","label":"Archcraft","url":"https://archcraft.io/"}]},
    {"id":"BBxKjisvYsHLFDDt","label":"daily","bookmarks":[{"id":"NalxVGifsTaQMAfo","label":"10fastfingers","url":"https://10fastfingers.com/"},{"id":"hZJVcQPw97pmF02J","label":"typeracer","url":"https://play.typeracer.com/"},{"id":"8g3r0pDxpoSOHDv5","label":"typeingtest","url":"https://www.typingtest.com/"},{"id":"uE0FY0LskILX9BuM","label":"test2io","url":"https://typetest.io/"}]},
{"id":"e2I70cjxZZxERhXE","label":"Courses","bookmarks":[{"id":"sGdPvbqCDcpPrbtb","label":"Cs50 (2021)","url":"https://www.youtube.com/playlist?list=PLhQjrBD2T383f9scHRNYJkior2VvYjpSL"},{"id":"bn6fKQyioBescM8v","label":"Cs50 (2020)","url":"https://www.youtube.com/playlist?list=PLhQjrBD2T382_R182iC2gNZI9HzWFMC_8"},{"id":"Pk6FsprtrzcbXuD4","label":"Python","url":"https://www.youtube.com/playlist?list=PL-osiE80TeTt2d9bfVyTiXJA-UTHn6WwU"},{"id":"Az84TazW68ofz5w0","label":"C++","url":"https://www.youtube.com/watch?v=vLnPwxZdW4Y&t=622s"}]}]

const createGroupContainer = () => {
  const container = document.createElement("div")
  container.className = "bookmark-group"
  return container
}

const createGroupTitle = title => {
  const h2 = document.createElement("h2")
  h2.innerHTML = title
  return h2
}

const createBookmark = ({ label, url }) => {
  const li = document.createElement("li")
  const a = document.createElement("a")
  a.href = url
  a.innerHTML = label
  li.append(a)
  return li
}

const createBookmarkList = bookmarks => {
  const ul = document.createElement("ul")
  bookmarks.map(createBookmark).forEach(li => ul.append(li))
  return ul
}

const createGroup = ({ label, bookmarks }) => {
  const container = createGroupContainer()
  const title = createGroupTitle(label)
  const bookmarkList = createBookmarkList(bookmarks)
  container.append(title)
  container.append(bookmarkList)
  return container
}

const injectBookmarks = () => {
  const bookmarksContainer = document.getElementById("bookmarks")
  bookmarksContainer.append()
  bookmarks.map(createGroup).forEach(group => bookmarksContainer.append(group))
}

injectBookmarks()
