/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */

/**
 * Search function
 */

const searchInput = document.querySelector("#searchbar > input")
const searchButton = document.querySelector("#searchbar > button")

const lookup = {"/":"/","deepl":"https://deepl.com/","reddit":"https://reddit.com/","maps":"https://maps.google.com/"}
const engine = "searx"
const engineUrls = {
  deepl: "https://www.deepl.com/translator#-/-/",
  duckduckgo: "https://duckduckgo.com/?q=",
  ecosia: "https://www.ecosia.org/search?q=",
  google: "https://www.google.com/search?q=",
  startpage: "https://www.startpage.com/search?q=",
  youtube: "https://www.youtube.com/results?q=",
  searx: "https://searx.be/search?q="
}

const isWebUrl = value => {
  try {
    const url = new URL(value)
    return url.protocol === "http:" || url.protocol === "https:"
  } catch {
    return false
  }
}

const getTargetUrl = value => {
  if (isWebUrl(value)) return value
  if (lookup[value]) return lookup[value]
  return engineUrls[engine] + value
}

const search = () => {
  const value = searchInput.value
  const targetUrl = getTargetUrl(value)
  window.open(targetUrl, "_self")
}

searchInput.onkeyup = event => event.key === "Enter" && search()
searchButton.onclick = search

/**
 * inject bookmarks into html
 */

const bookmarks = [{"id":"sDOcBtcmLaiZHzTk","label":"reddit","bookmarks":[{"id":"qKFO3gsLXV5bFUmM","label":"r/startpages","url":"https://www.reddit.com/r/startpages/"},{"id":"3IVibR3V97IO3Wel","label":"r/typescript","url":"https://www.reddit.com/r/typescript/"},{"id":"jHNVinGWn808VOJO","label":"r/reactjs","url":"https://www.reddit.com/r/reactjs/"}]},{"id":"XaRuLwfTByminTEV","label":"design tools","bookmarks":[{"id":"sGiBAUwESzWONAmu","label":"pixlrx","url":"https://pixlr.com/x/"},{"id":"HQ0p8VXMiciMNMLE","label":"image enlarger","url":"https://bigjpg.com/en"},{"id":"2A6g8GrL8Pm6yyQT","label":"haikei","url":"https://app.haikei.app/"},{"id":"vNvtXncpjDwiIxyn","label":"css gradients","url":"https://larsenwork.com/easing-gradients/"}]},{"id":"eJu9vBwytnv1Fmg1","label":"worth reading","bookmarks":[{"id":"b8CG3q7QcFKdOtry","label":"happy hues","url":"https://www.happyhues.co/"},{"id":"yHOHygonUVxvrWcm","label":"styled-components","url":"https://www.joshwcomeau.com/react/demystifying-styled-components/"},{"id":"5rmL8oAwmaGlq7yM","label":"react docs","url":"https://reactjs.org/docs/getting-started.html"}]},{"id":"16Ln1vEAkHZctk4z","label":"sources","bookmarks":[{"id":"1YU9DQnPTsUolKhO","label":"icons","url":"https://feathericons.com/"},{"id":"GVandlWxXXAVUCmR","label":"gif","url":"https://designyoutrust.com/2019/05/the-chill-and-retro-motion-pixel-art-of-motocross-saito/"},{"id":"MBIRsjCBzSpE8Pjw","label":"@startpage","url":"https://prettycoffee.github.io/startpage"},{"id":"UoHpv7lBlNIDWyjw","label":"author","url":"https://prettycoffee.github.io/"}]}]

const createGroupContainer = () => {
  const container = document.createElement("div")
  container.className = "bookmark-group"
  return container
}

const createGroupTitle = title => {
  const h2 = document.createElement("h2")
  h2.innerHTML = title
  return h2
}

const createBookmark = ({ label, url }) => {
  const li = document.createElement("li")
  const a = document.createElement("a")
  a.href = url
  a.innerHTML = label
  li.append(a)
  return li
}

const createBookmarkList = bookmarks => {
  const ul = document.createElement("ul")
  bookmarks.map(createBookmark).forEach(li => ul.append(li))
  return ul
}

const createGroup = ({ label, bookmarks }) => {
  const container = createGroupContainer()
  const title = createGroupTitle(label)
  const bookmarkList = createBookmarkList(bookmarks)
  container.append(title)
  container.append(bookmarkList)
  return container
}

const injectBookmarks = () => {
  const bookmarksContainer = document.getElementById("bookmarks")
  bookmarksContainer.append()
  bookmarks.map(createGroup).forEach(group => bookmarksContainer.append(group))
}

injectBookmarks()
